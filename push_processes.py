import datetime
import json

import numpy as np
import requests
from numpy import random

from helper.ppm import post_to_ppm, to_ppm_iso, t_series

filepath = './data/HEAD_cycle_data_evaluation_1_25000.csv'

d = np.genfromtxt(filepath, delimiter=',', names=True,dtype=None, encoding='utf8')
#only for one cavity
d = d[::8][:].copy()
vw = d.view(dtype=np.float64).reshape(len(d), -1)



num_curves = vw.shape[0]

all_y = vw[:, -(t_series.shape[0] + 1):-1]
max_y = all_y.max()
min_y = all_y.min()

for n in range(30):
    # n = 100
    if n % 50 == 0:
        print('pushing process {} of {}'.format(n, num_curves))
    row = vw[n, :]

    isok = row[-1] == 1
    y = row[-(t_series.shape[0] + 1):-1]
    # t = to_ppm_iso(d[n][1])
    t = datetime.datetime.now().isoformat() + '+02:00'
    endpoint = "process"

    payload = {
        "content-spec": "urn:spec://eclipse.org/unide/process-message#v3",
        "device": {
            "id": "matebabos-ij1",
        },
        "process": {
            "externalId": "p{}".format(n + random.random()),
            # "result": 'OK' if isok else 'NOK',
            "ts": t,
        },
        "measurements": [
            {
                "name": "processing",
                "phase": "pase 1",
                # "result": 'OK' if isok else 'NOK',
                "series": {
                    "time": [int(i) for i in list(t_series*1000)], #[0:100],
                    "pressure": list(y), #[0:100],
                },
                "ts": t
            }
        ]
    }
    response = post_to_ppm(endpoint, payload)
    print(response.status_code)

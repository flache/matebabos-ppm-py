import datetime
import json

import numpy as np
import requests

headers = {
    'Content-Type': "application/json",
}

def post_to_ppm(endpoint, payload):
    return requests.request(
        "POST",
        'https://demo.ppm.bosch-si.com/ppm/v3/{}'.format(endpoint),
        data=json.dumps(payload),
        headers=headers,
        verify=False,
        cert=(
            '/Users/fl/workspace/matebabos_sammlunng/client29/azurevpn_bci_client29.crt',
            '/Users/fl/workspace/matebabos_sammlunng/client29/azurevpn_bci_client29.pem',
        ),
    )

def to_ppm_iso(t_string):
    d = datetime.datetime.strptime(t_string, '%Y%m%dT%H%M%S.%f')
    d = d.replace(year=d.year+2, month=d.month-9)
    return d.isoformat() + 'Z'


t_series = np.arange(0, 2, 0.002) * 5

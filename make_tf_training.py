import numpy as np
import matplotlib.pyplot as plt

filepath = './data/HEAD2_cycle_data_evaluation_1_25000.csv'

OUT_DIR = './tfp'
d = np.genfromtxt(filepath, delimiter=',', names=True)
vw = d.view(np.float64).reshape(len(d), -1)

num_curves = vw.shape[0]

all_y = vw[:, -(t_series.shape[0]+1):-1]
max_y= all_y.max()
min_y = all_y.min()

fig = plt.figure(
        frameon=False,
    )

labels = vw[:, -1]
np.savetxt('{}/labels.txt'.format(OUT_DIR), labels, delimiter=',',fmt='%0.0f')
# for n in range(num_curves):
for n in range(5,15):
    if n%50 == 0:
        print('Creating figure {} of {}'.format(n, num_curves))
    row = vw[n, :]
    evaluation = row[-1]
    y = row[-(t_series.shape[0]+1):-1]


    fig.clf()
    ax = fig.add_axes([0, 0, 1, 1])
    # ax.axis('off')

    ax.plot(t_series, y, color='black')

    ax.set_ylim(min_y, max_y)
    ax.set_xlim(0, 2)
    # plt.axis([None, None, min_y, max_y])
    # plt.show()
    fig.savefig('{}/plt_{}.png'.format(OUT_DIR, n), dpi=20)